package br.edu.iftm.restapp.service;

import java.util.List;

import br.edu.iftm.restapp.entity.Post;
import br.edu.iftm.restapp.entity.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiEndPoint {
    @GET("users")
    Call<List<User>> obterUsuarios();

    @POST("posts")
    Call <Post> criarPost(@Body Post post);
}
