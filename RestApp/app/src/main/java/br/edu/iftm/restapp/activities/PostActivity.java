package br.edu.iftm.restapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import br.edu.iftm.restapp.R;
import br.edu.iftm.restapp.entity.Post;
import br.edu.iftm.restapp.service.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostActivity extends AppCompatActivity {
    private static final String TAG = "PostActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        enviarConsultaRest();
    }

    private void enviarConsultaRest() {
        Post post = new Post(1, "Teste" ,
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vehicula hendrerit dolor, a pretium nunc molestie porttitor. Aenean laoreet efficitur est, eu auctor orci posuere at. Nulla scelerisque commodo feugiat. Nam vel rutrum nibh. In quis risus sit amet mauris efficitur imperdiet. Curabitur nec rutrum lacus. Donec dignissim condimentum aliquet. Ut orci magna, pharetra in accumsan sit amet, elementum non mi. Maecenas ac interdum lorem, eu interdum arcu. Aenean urna justo, tristique eu dictum in, venenatis a quam. Sed efficitur elementum risus, sed bibendum orci porttitor sagittis. Praesent ante magna, lobortis dapibus quam sed, tincidunt aliquam eros.");
        RetrofitService.getServico().criarPost(post).enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                int cod = response.code();
                int id = response.body().getId();
                ((TextView)findViewById(R.id.textViewNovoIdCriado)).setText(""+id);
                ((TextView)findViewById(R.id.textViewCodigoRetorno)).setText(""+cod);
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Log.d(TAG, "onFailue: "+t.getMessage());
            }
        });
    }
}
